from random import randrange
from unidecode import unidecode

def open_file(name, mode):
    """
    Fichier utilisé pour ouvrir le fichier avec le nom et le mode donnée en paramètre. Attrape les exceptions
    si il y a un problème
    """
    liste_mot = []
    try:
        with open(name, mode, encoding="ISO-8859-1") as fichier:
            for line in fichier:
                liste_mot.append(line.strip())
            return liste_mot
    except OSError as err:
        print("OS error: {0}".format(err))
        return 0
    except ValueError as err:
        print("Value Error: {0}".format(err))
        return 0

def mot_random():
    """
    Retourne un mot aléatoire parmis la liste des mots du fichier
    """
    liste = open_file("dico.txt", "r")
    mot_aleatoire = unidecode(liste[randrange(len(liste))].lower())
    return mot_aleatoire

def mot_convertis_etoiles(mot):
    """
    Convertis un mot passé en paramètre en étoiles
    """
    new_mot = ""
    i = 0
    while i < len(mot):
        new_mot += "*"
        i += 1
    return new_mot
def etoiles_convertis_lettre(lettre, mot, mot_etoiles):
    """
    Si la lettre définie en paramètre apparait dans le mot,
    alors on change l'étoiles à la place correspondante dans
    le mot étoiles
    """
    new_mot = ""
    i = 0
    while i < len(mot):
        if mot[i] == lettre:
            new_mot += lettre
        else:
            new_mot += mot_etoiles[i]
        i += 1
    return new_mot