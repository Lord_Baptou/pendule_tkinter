import tkinter as tk
from PIL import Image
from PIL import ImageTk
from tkinter import messagebox
import functions as functions

class Application(tk.Tk):
    def __init__(self, master=None):
        super().__init__()
        self.geometry("500x500+700+250") #La fenetre fait 500 par 500 et sera positionné au coordonée 700:250 de l'écran
        self.title("Jeu du Pendu")
        self.config(bg="bisque")

        self.play()

    def play(self):
        for widget in self.winfo_children(): #Destruction de tout les widgets dans la fenetre pour commencer une nouvelle partie
            widget.destroy()
        self.frame = tk.Frame(self, width=500, height=500) #Création d'une frame de hauteur 500 par 500
        self.frame.config(bg="bisque")
        self.frame.pack()

        self.init_image()
        self.can = tk.Canvas(self.frame, width = self.image.size[0], height=self.image.size[1]-100) #Création d'un canvas dans la frame au dimension des images
        self.can.pack()
        self.image_on_canvas = self.can.create_image(0,0, anchor=tk.NW, image = self.my_images[self.my_image_number]) #Création des images dans le canvas au position 0,0

        self.init()

        self.lettre_used = []
        tk.Label(self.frame, text = "Lettre utilisé: ", bg="bisque").pack()
        self.lettre_used_label = tk.Label(self.frame, text = self.lettre_used, bg = "bisque")
        self.lettre_used_label.pack()

        self.quit_button = tk.Button(self.frame, text="Quitter", command=self.destroy).pack(side="left")
        self.play_again_button = tk.Button(self.frame, text="Rejouer", command=self.play).pack(side="right")
        
        
        
    def valider(self):
        """
        -Fonction qui sert à modifier le mot mystère en fonction de la lettre envoyé
        -Update des images en fonctions
        """
        self.lettre_get = self.lettre.get().lower() #Récupère la lettre en minuscule
        self.lettre_entry.delete(0, tk.END)
        
        self.lettre_used.append(self.lettre_get)
        self.lettre_used_label.config(text = self.lettre_used)

        self.old_mot_etoiles = self.mot_etoiles #stockage dans une variable auxiliaire
        self.mot_etoiles = functions.etoiles_convertis_lettre(self.lettre_get, self.mot, self.mot_etoiles)

        if self.old_mot_etoiles == self.mot_etoiles:  #Si la variable est la même que le mot etoiles (cf la lettre pas dans le mot), alors on change l'image
            self.my_image_number += 1
            if self.my_image_number == len(self.my_images) - 1: #Si on arrive à la fin des images, on perd

                messagebox.showwarning("Perdu", "Vous avez perdu, le bon mot était: "+self.mot)
                
            self.can.itemconfig(self.image_on_canvas, image = self.my_images[self.my_image_number])

        else: #Sinon on update le mot etoiles
            self.mot_etoiles_label.config(text = self.mot_etoiles)
        
        if self.mot_etoiles == self.mot: #Si le mot etoile et le même que le mot de départ, on gagne
            messagebox.showinfo("Gagné", "Bravo vous avez trouvé le mot mystère.")
        
    def init_image(self):
        """
        Création de la liste d'image
        """
        self.my_images = []
        self.my_image_number = 0
        for i in range(12):
            nom_fichier = "img/pendu_"+str(i)+".png"
            self.image = Image.open(nom_fichier)
            self.my_images.append(ImageTk.PhotoImage(self.image))

    def init(self):
        """
        Fonction d'affichage du mot mystère (cf mot_etoiles)
        """
        self.mot = functions.mot_random()
        self.mot_etoiles = functions.mot_convertis_etoiles(self.mot)
        tk.Label(self.frame, text="Vous devez deviner ce mot:", bg="bisque").pack()
        self.mot_etoiles_label = tk.Label(self.frame, text = self.mot_etoiles, bg="bisque")
        self.mot_etoiles_label.pack()
        
        self.ligne_saisi()
        
    
    def ligne_saisi(self):
        """
        Fonction pour récupérer la lettre envoyé
        """
        tk.Label(self.frame, text = "Entrez votre lettre: ", bg="bisque").pack()
        self.lettre = tk.StringVar()
        self.lettre_entry = tk.Entry(self.frame, textvariable=self.lettre, bg="bisque")
        self.lettre_entry.pack()

        self.send_lettre = tk.Button(self.frame, text="Envoyer", command=self.valider)
        self.send_lettre.pack()


if __name__ == '__main__':
    
    app = Application(tk.Tk)
    app.mainloop()
    exit(0)