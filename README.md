# pendu_tkinter

Voici un petit jeu de pendu utilisant Tkinter en Python.
Pour le bon fonctionnement du jeu, vous devez avoir sur votre pc:

-Python (obvious)
-Le modul PIL 
-Le modul unidecode

Pour installer ces modules, faîtes simplement depuis un terminal de commande:

sudo pip install "nom_du_module" (Linux)
pip install "nom_du_module" (Windows)

Si vous n'avez pas pip d'installer, éxecuter ces commandes:

curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py

Bon jeu :)

Made by Baptiste Leroux